def data_type(n):

    #check for string data type
    if type(n) == str:
        return len(n)

    #check for None
    elif type(n) == None:
        return "No Value"

    #check for bool data type
    elif type(n) == bool:
        if n:
            return "True"
        else:
            return "False"

    #check for int data type
    elif type(n) == long:
        if n > 100:
            return "More than 100"
        elif n < 100:
            return "Less than 100"
        elif n == 100:
            return "Equal to 100"

    #check for list
    elif type(n) == list:
        if len(n) >= 3:
            return n[2]
        else:
            return "None"


