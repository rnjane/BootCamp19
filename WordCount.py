

def words(line):
    '''Method to count occurences of words in a sentence'''
    wordes = line.split()
    word_occurence = dict()

  #Loop through a sentence to find occurences
    for word in wordes:
        if word in word_occurence.keys():
            word_occurence[word] += 1
        elif word.isdigit():
            word_occurence[int(word)] = 1
        else:
            word_occurence[word] = 1

    return word_occurence


print(words("Hello Hello Maggie"))
