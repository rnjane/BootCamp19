

class Animal:
    '''Class Initialization'''
    #data hiding(private data members)
    __name = ""
    __sound = ""
    __weight = 0
    __height = 0

    def __init__(self, name, sound, weight, height):
        self.__name = name
        self.__sound = sound
        self.__weight = weight
        self.__height = height


    #Encapsulation - achieved by using getters and setters
    def setname(self, name):
        '''Set name'''
        self.__name = name

    def getname(self):
        '''Get name'''
        return self.__name

    def setsound(self, sound):
        '''set sound'''
        self.__sound = sound

    def getsound(self):
        '''get sound'''
        return self.__sound

    def setheight(self, height):
        '''set height'''
        self.__height = height

    def getheight(self):
        '''get height'''
        return self.__height

    def setweight(self, weight):
        '''set weight'''
        self.__weight = weight

    def getweight(self):
        '''get weight'''
        return self.__weight

    def gettype(self):
        '''get type'''
        print("Animal")

MERINHO = Animal("Sheep", "Meee", 12, 25)


class Dog(Animal):
    '''inheritance Dog class inheriting from animal class'''
    __owner = ""

    def __init__(self, owner, name, sound, weight, height):
        self.__owner = owner
        super(Dog, self).__init__(name, sound, weight, height)

    def gettype(self):
        print("Dog")

    def multiplesounds(self, howmany=None):
        '''method overloading'''
        if howmany is None:
            print(self.getsound())
        else:
            print(self.getsound() * howmany)


SIMBA = Dog("Roba", "Simba", "Bark", 50, 250)


class AnimalType:
    def gettype(self, Animal):
        '''get type'''
        Animal.gettype()

ANIMAL1 = AnimalType()
#polymorphism here
ANIMAL1.gettype(MERINHO)
ANIMAL1.gettype(SIMBA)
