

def fizz_buzz(num):
    '''Check if input is divisible by 3 and 5'''
    if num % 3 == 0 and num % 5 == 0:
        return 'FizzBuzz'

    #Check if input is divisible by 3
    if num % 3 == 0:
        return 'Fizz'

    #Check if input is divisibke by 5
    if num % 5 == 0:
        return 'Buzz'

    #Check if input is not divisible by 3 or 5
    if not(num % 3 == 0 or num % 5 == 0):
        return num
        