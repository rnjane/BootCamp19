

class Car(object):
    '''Class initialization'''
    num_of_doors = 4
    num_of_wheels = 4

    '''Constructor for the car class, with initializations'''
    def __init__(self, name='General', model='GM', ctype='saloon', speed=0):
        self.name = name
        self.model = model
        self.ctype = ctype
        self.speed = speed

        '''Assign number of doors and wheels according to car type'''
        if self.name == 'Porshe' or self.name == 'Koenigsegg':
            self.num_of_doors = 2
        elif self.ctype == 'trailer':
            self.num_of_wheels = 8

    def is_saloon(self):
        '''Method to check if type of car is a saloon'''
        if self.ctype is not'trailer':
            return True
        return False

    def drive(self, speed):
        '''Method to check type of car and return its speed'''
        if self.ctype is 'trailer':
            self.speed = speed * 77/speed
        else:
            self.speed = 10 ** speed
        return self
