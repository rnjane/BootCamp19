
def find_max_min(num_list):
    '''returns max and min numbers, or the size of the list in case max and min are equal'''
    size = len(num_list)
    lowest = num_list[0]
    highest = num_list[0]

    #Loops to find max and min numbers
    for i in range(0, size):
        if num_list[i] > highest:
            highest = num_list[i]
    for k in range(0, size):
        if num_list[k] < lowest:
            lowest = num_list[k]

    #Return size of list if max = min
    if highest == lowest:
        return [len(num_list)]
    else:
        return [lowest, highest]


print(find_max_min([1, 2, 3, 4, 5, 6]))
