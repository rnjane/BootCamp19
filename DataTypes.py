

def data_type(name):

    #check for string data type
    if type(name) == str:
        return len(name)

    #check for None
    elif name  is None:
        return 'no value'

    #check for bool data type
    elif type(name) == bool:
        return name

    #check for int data type
    elif type(name) == int:
        if name < 100:
            return "less than 100"
        elif name == 100:
            return "Equal to 100"
        else:
            return "more than 100"

    #check for list
    elif type(name) == list:
        if len(name) < 3:
            return None
        else:
            return name[2]
