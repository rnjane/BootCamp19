## About The Repository

This repository contains labs for the Andela Kenya bootcamp, week 1.

The labs are written in python 3.4.

You will need requests extension to run one of the labs.
