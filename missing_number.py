def list_to_int(mylist):
    '''function to convert a list to integer'''
    new_int = map(str, mylist)
    new_int = ''.join(new_int)
    new_int = int(new_int)
    return new_int

def find_missing(array_1, array_2):
    '''function to find missing number between two arrays, and return it'''
    size_1 = len(array_1)
    size_2 = len(array_2)
    if size_1 > size_2:
        missing_element = list(set(array_1) - set(array_2))
        return list_to_int(missing_element)
    elif size_2 > size_1:
        missing_element = list(set(array_2) - set(array_1))
        return list_to_int(missing_element)
    elif size_2 == 0 and size_1 == 0:
        return 0
    elif size_1 == size_2:
        return 0

print(find_missing([1, 2, 3, 4, 5, 6], [1, 2, 3, 4]))
