

import requests

def get_user():
    '''This is a simple CLI python implementation of GitHub API'''
    #get username from the user
    username = input("What is your user name: >>")

    #Fetch Public Info of the user from GitHub
    profile_info = requests.get('https://api.github.com/users/' + username)
    data = profile_info.json()

    #Return only Location, Followers, E-Mail and Public Repositories
    info = data['location'], data['followers'], data['email'], data['public_repos']

    #Add the data to a list and print all
    info = list(info)
    print("Location: %s Followers: %d Email: %s Public Repository: %d" % (info[0], info[1], info[2], info[3]))


#Method Call
get_user()
